\documentclass[a4paper,12pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} % använd utf8
\usepackage[swedish]{babel} % använd svenska

\usepackage{csquotes} % sitat-tecken funktion \enquote{}
\usepackage{mathtools} % matte
\usepackage{amsfonts} % matte-font

\usepackage{graphicx} % bild paketet 
\usepackage{float}
\graphicspath{ {images/} } % bild mapp 

\usepackage{lastpage} % egen header och footer
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{Rapport 4 - RC-filter}
\chead{EITA40}
\rhead{ma7333fa-s@student.lu.se}
\cfoot{Sida \thepage\ av \pageref{LastPage}}

\title{Laborationsrapport\\ Nr 4 -- RC-filter\\ EITA40 Krets- och mätteknik \noindent\rule{\textwidth}{1pt}}
\author{Laboranter: Max Faxälv\\ Handledare: Andreas Johansson}
\date{\vspace{2pt}Datum: 2018-05-09\\ Uppdaterad: 2020-02-14}

\begin{document}

\maketitle

\newpage


\section{Inledning}

RC-filter är ett filter som antingen filtrerar bort låga eller höga frekvensker av en signal. Syftet med den här laborationen var att ge förståelse för hur ett RC-filter fungerar, samt att ge erfarenhet av mätning med oscilloskop.

\section{Teori}

RC-filter (Resistance-Capacitor) resistans-kapacitans filter är indelad i högpass- och lågpass-filter, beroende på vilken konfiguration motståndet och kapacitatorn har. Lågpassfiltret släpper bara igenom låga frekvenser och filtrerar bort höga frekvensker, medan högpassfitler har motsatt effekt. \cite{elkretsteori}

\section{Metod}

Komponenter som användes i labben:

\begin{itemize} 
\item Resistor
\item Kondensator
\item Funktionsgenerator
\item Oscilloskop
\item Kopplingsmaterial (sladdar, krokodilklämmor, breadboard)
\end{itemize}

\subsection{Lågpassfilter}

Ett lågpassfilter kopplades upp enligt kretsschemat i figur \ref{fi:lp} på kopplingsplattan. Probarna kalibrerades på x10 dämning och kopplades till kretsens utsignal. På kretsschemat är punkten till vänster insignalen, och punkten längst till höger den filtrerade utsignalen. Kondensatorn som går nedåt är kopplad till jord. Mätningar togs sedan på 6 olika frekvenser med en 5V sinusformad insignal.

\begin{figure}[H]
	\centering \includegraphics[width=200px]{lp}
	\caption{Kretsschema av lågpassfilter. \cite{kretsscheman}}
	\label{fi:lp}
\end{figure}

Dämpningen i decibel samt fasförskutningen i grader räknades ut med hjälp av följande formlerna:

\begin{equation}
	\mathbf{H(\omega)} = \frac{K}{1 + j \frac{\omega}{\omega{}_b}}
\end{equation}
\begin{equation}
	\mathbf{|H(\omega)|_{dB}} = 20 \cdot \log{K} - 20 \cdot \bigg(\sqrt{1 + \Big(\frac{\omega}{\omega_b}\Big)^2}\bigg)
\end{equation}

där $K$ är dämpningen till filtret och $\omega_b$ är

\begin{equation} \label{eq:wb}
	\omega_b = \frac{1}{RC}
\end{equation}

där $R=10k \Omega$ och $C=1nF$. Fasförskutning i grader togs fram med följande formel:

\begin{equation} \label{eq:fg}
	arg(\mathbf{H(\omega)})[^\circ] = Dt \cdot f \cdot 360^\circ
\end{equation}

där $Dt$ är tidsdifferansen mellan orginalsignalen och signalen ut av filtret. Med $X_C$ som är mätvärdet över kondensatorn så är den teoretiska formelen för fasförskjutningen: \cite{teoform}

\begin{equation} \label{eq:fgt}
	\phi = arg(\mathbf{H(\omega)})[^\circ] = -\tan^{-1} (\frac{R}{X_C})
\end{equation}

\subsection{Högpassfilter}

Ett högpassfilter kopplades upp enligt kretsschemat i figur \ref{fi:hp} på kopplingsplattan. Probarna kalibrerades återigen på x10 dämning och kopplades till kretsens utsignal. På kretsschemat är punkten till vänster insignalen, och punkten längst till höger den filtrerade utsignalen. Resistorn som går nedåt är kopplad till jord. Mätningar togs sedan på 6 olika frekvenser med en 5V sinusformad insignal.

\begin{figure}[H]
	\centering \includegraphics[width=200px]{hp}
	\caption{Kretsschema av högpassfilter. \cite{kretsscheman}}
	\label{fi:hp}
\end{figure}

Dämpningen i decibel samt fasförskutningen i grader räknades ut med hjälp av följande formlerna:

\begin{equation}
	\mathbf{H(\omega)} = K \cdot \frac{j \frac{\omega}{\omega_b}}{1 + j \frac{\omega}{\omega_b}}
\end{equation}
\begin{equation}
	\mathbf{|H(\omega)|_{dB}} = 20 \cdot \log{K} + 20 \cdot \log{\Big(\frac{\omega}{\omega_b}\Big)} - 20 \cdot \bigg(\sqrt{1 + \Big(\frac{\omega}{\omega_b}\Big)^2}\bigg)
\end{equation}

där $\omega_b$ är samma som i formel \ref{eq:wb}. Fasförskutningen räknas ut på samma sätt som i formel \ref{eq:fg}.

\section{Resultat}

\subsection{Uppgift 1}

Följande avsnitt presenterar de teoretiska samt de mätta värderna för ett lågpassfilter med en $10k\Omega$ kondensator samt en $1nF$ motstånd. Det är möjligt att se utifrån graferna att de uppmätta värderna stämmer överrens med de uträknade teoretiska värderna, med hänssyn till mindre avrudning och mättfel som uppstår i verkligheten.

\begin{table}[H]
\centering
	\def\arraystretch{1.7}
	\begin{tabular}{ c | c | c | c | c }
	\hline
	\textbf{Frekvens [Hz]} & \textbf{Utsignal [V]} & \textbf{|H($\omega$)|$\frac{Uut}{Uin}$[ggr]} & \textbf{|H(w)|dB[dB]} & \textbf{arg(H(w))[$^{\circ}$]} \\ \hline
	159 & 5V & 1 & 0 & 0$^{\circ}$ \\ \hline
	1590 & 5V & 1 & 0 & 0$^{\circ}$ \\ \hline
	7950 & 4,6V & 0,9 & -1 & -28$^{\circ}$ \\ \hline
	31800 & 2,3V & 0,46 & -6 & -64$^{\circ}$ \\ \hline
	159000 & 0,5V & 0,1 & -20 & -86$^{\circ}$ \\ \hline
	1590000 & 0,05V & 0,01 & -40 & -87$^{\circ}$ \\ \hline
	\end{tabular}
	\caption{Tabellen visar resultatet av mätningarna från lågpassfiltret.}
	\label{table:res1}
\end{table}

\begin{figure}[H]
	\label{fi:lpdb}
	\includegraphics[width=200px]{lpdb}
	\label{fi:lpg}
	\includegraphics[width=200px]{lpg}
	\caption{Graferna visar de mätta värderna presenterade i tabellen \ref{table:res1}.}
\end{figure}

\begin{figure}[H]
	\centering
	\label{fi:lpt}
	\includegraphics[width=300px]{lpt}
	\caption{Grafen visar de teoretiskt uträknade värderna för lågpassfiltret.}
\end{figure}

\subsection{Uppgift 2}

Följande avsnitt presenterar de teoretiska samt de mätta värderna för ett högpassfilter med en $1nF$ motstånd och en $10k\Omega$ kondensator. Det är möjligt att se utifrån graferna att de uppmätta värderna stämmer överrens med de uträknade teoretiska värderna, med hänssyn till mindre avrudning och mättfel som uppstår i verkligheten.

\begin{table}[H]
\centering
	\def\arraystretch{1.7}
	\begin{tabular}{ c | c | c | c | c }
	\hline
	\textbf{Frekvens [Hz]} & \textbf{Utsignal [V]} & \textbf{|H($\omega$)|$\frac{Uut}{Uin}$[ggr]} & \textbf{|H(w)|dB[dB]} & \textbf{arg(H(w))[$^{\circ}$]} \\ \hline
	159 & 0.05V & 0,01 & -40 & -89$^{\circ}$ \\ \hline
	1590 & 0.5V & 0,1 & -20 & -86$^{\circ}$ \\ \hline
	7950 & 2,2V & 0,44 & -7 & -66$^{\circ}$ \\ \hline
	31800 & 4,5V & 0,9 & -1 & -27$^{\circ}$ \\ \hline
	159000 & 5V & 1 & 0 & 0$^{\circ}$ \\ \hline
	1590000 & 5V & 1 & 0 & 0$^{\circ}$ \\ \hline
	\end{tabular}
	\caption{Tabellen visar resultatet av mätningarna från högpassfiltret.}
	\label{table:res2}
\end{table}

\begin{figure}[H]
	\label{fi:hpdb}
	\includegraphics[width=200px]{hpdb}
	\label{fi:hpg}
	\includegraphics[width=200px]{hpg}
	\caption{Graferna visar de mätta värderna presenterade i tabellen \ref{table:res2}.}
\end{figure}

\begin{figure}[H]
	\centering
	\label{fi:hpt}
	\includegraphics[width=300px]{hpt}
	\caption{Grafen visar de teoretiskt uträknade värderna för högpassfiltret.}
\end{figure}

\newpage
\section{Diskussion}

Från tabellerna och dom genererade graferna i resultatet kan man se att den uppkopplade kretskonfiguration möjligör en filtrering av låga eller höga frekvensker. Brytfrekvensen till RC-filtret beror på resistorns och kondensatorns storlek. Anledningen till att lågpassfilter filtrerar bort höga frekvenser är p.g.a.~att med låga frekvenser hinner kondensatorn inta en ladning som sedan hidrar strömmen att passera, och går därför till \enquote{ut}. Med höga frekvenser går all ström till att ladda och ladda-ur kondensatorn och därmed effektivt \enquote{dämpar} dom högre frekvenserna. I ett högpassfilter med låga frekvenser intar kondensatorn en stabil ladning och hindrar därmed signalen från att passera, men med höga frekvenser laddas den ur fortare och kan därmed skicka signalen vidare. En kondensator kan därmed också alltså användas som en DC blockerare, i och med att ingen ström flyter igenom när den väl är laddad, vilket inte är tillfället med AC eftersom spänningen flukturerar.

\begin{thebibliography}{9}
	\bibitem{kretsscheman}
		OKAWA Electric Design. Filter Design and Analysis, bilder på kretsschema för RC-filter.
		\\\texttt{http://sim.okawa-denshi.jp/en/Fkeisan.htm}

	\bibitem{elkretsteori} 
		A. Alfredsson och R.K. Rajput. 
		\textit{Elkretsteori}. 
		Liber AB, 2009.

	\bibitem{eita50for17}
		Andreas Johansson. Föreläsning 17: HP \& LP filter, Bodediagram, Lunds Universitet. \\
		\texttt{https://www.eit.lth.se/fileadmin/eit/courses/eita40/
		lectures/17\_f\%F6rel\%E4sning\_eita40.pdf}

	\bibitem{teoform}
		Anders Nivstrand, Arvid Norberg, Pernilla Norlund, Frank Norrman, Ralf Nyrén, Fredrik Näslund, Göran Sandström. \\
		2000-05-15. Låg-pass filter, TDV00, Umeå Universitet. \\
		\texttt{http://www8.tfe.umu.se/courses/elektro/anakrets/tdv00/html/
		grupp7/filter/low-pass/low-pass.shtml}
\end{thebibliography}

Referens \cite{eita50for17} är inte längre tillgänlig. Flera formler hämtades från denna källa, men p.g.a. ej möjlighet att att åter öppna källan går det inte att gå tillbaka och korrekt citera vilka formler som kommer från föreläsningsmaterialet och vilka kommer från boken.


\end{document}
